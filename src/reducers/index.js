import loginReducer from "./login.reducer";
import homeReducer from "./home.reducer";
import commonReducer from "./common.reducer";

export {
	loginReducer, 
	homeReducer, 
	commonReducer,
};
