import { combineReducers } from "redux";
import assign from "lodash/assign";
import { RECEIVE_HOME_DATA } from "@types";

const homeState = (state = null, action) => {
	switch (action.type) {
		case RECEIVE_HOME_DATA:
			return assign({}, state, {
				...action.payload,
			});
		default:
			return state;
	}
};

const homeReducer = combineReducers({
	homeState,
});

export default homeReducer;
