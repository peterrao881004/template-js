import { combineReducers } from "redux";
import assign from "lodash/assign";
import { RECEIVE_LOGIN_DATA } from "@types";

const loginState = (state = null, action) => {
	switch (action.type) {
		case RECEIVE_LOGIN_DATA:
			return assign({}, state, {
				...action.payload,
			});
		default:
			return state;
	}
};

const loginReducer = combineReducers({
	loginState,
});

export default loginReducer;
