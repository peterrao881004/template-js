
import { combineReducers } from "redux";
import assign from "lodash/assign";
import { UPDATE_INITIALIZE, PAGE_INITIALIZED_ERROR } from "@types";

const initializeState = {
	errorType: 0,
	errorMsg: "",
	isInitialized: true,
};
const initStatus = (state = initializeState, action) => {
	switch (action.type) {
		case UPDATE_INITIALIZE:
		case PAGE_INITIALIZED_ERROR:
			return assign({}, state, {
				...action.payload,
			});
		default:
			return state;
	}
};

const commonReducer = combineReducers({
	initStatus,
});

export default commonReducer;