export const fetchInit = "GET ../mock_data/init.json";
export const fetchWorldData = "GET ../mock_data/world.json";

export const fetchHomeData = "GET ../mock_data/home.json";

export const fetchLoginData = "GET ../mock_data/login.json";
