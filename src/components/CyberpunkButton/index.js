import React from "react";
import PropTypes from "prop-types";
import './style.css'

const CyberpunkButton = (props) => {
  const { content, onClick } = props
  return <button onClick={() => {
    onClick && onClick()
  }}>{content}</button>
}

CyberpunkButton.propTypes = {
  content: PropTypes.string,
  onClick: PropTypes.func
};

export default CyberpunkButton;