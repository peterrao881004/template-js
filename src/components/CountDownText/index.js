import React, { useEffect, useState } from 'react'

const CountDownText = () => {
  const [count, setCount] = useState(0);
  useEffect(() => {
    const interval = window.setInterval(() => {
      setCount(x => {
        // if (x > 1) {
          return x + 1;
        // } else {
        //   return x;
        // }
      })
    }, 1000);
    return () => {
      window.clearInterval(interval);
    }
  }, [])
  return <span>已过去 {count} 秒</span>;
}

export default CountDownText;