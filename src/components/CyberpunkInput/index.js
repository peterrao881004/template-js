import React from "react";
import PropTypes from "prop-types";
import './style.scss'

const CyberpunkInput = (props) => {
    const { placeholder = 'input 输入框', type = 'input' } = props
    return type === 'input' ?
        <input className="cyberpunk" type="text" placeholder={placeholder} />
        : <textarea className="cyberpunk" placeholder={placeholder}></textarea>
}

CyberpunkInput.propTypes = {
    placeholder: PropTypes.string,
    type: PropTypes.string,
};

export default CyberpunkInput;