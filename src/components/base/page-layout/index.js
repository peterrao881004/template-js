import React from "react";
import { useStore } from "@hooks";
import PageError from "../page-error";

function Layout(props) {
	const { initStatus } = useStore("commonReducer");
	const { isInitialized, errorType, errorMsg } = initStatus;
	const renderContent = () => {
		// eslint-disable-next-line react/prop-types
		const WrappedComponent = props.component;

		return <WrappedComponent {...props} />;
	};
	return <>
		{
			isInitialized
				?
				errorType === 0
					?
					renderContent()
					:
					<PageError errorType={errorType} errorMsg={errorMsg} />
				:
				null
		}
	</>;
}

export default Layout;

