import React from "react";
import PropTypes from "prop-types";

function PageError(props) {
	const {
		errorType,
		// errorMsg
	} = props;

	// const config = () => {
	// 	switch (errorType) {
	// 		case 1: //通用出错了
	// 		default:
	// 			return {
	// 				type: "ds-error",
	// 				title: errorMsg || "出错了"
	// 			};
	// 		case 2: //网络超时
	// 			return {
	// 				type: "ds-error",
	// 				title: errorMsg || "网络超时",
	// 				desc: "网络不稳定，让我在努力一下"
	// 			};
	// 	}
	// };
	return (
		<div>
			{errorType === 2 && (
				<div className="custom-area-wrap">
					<button type="secondary" bg="white" border inline className="custom-btn-first" onClick={this.onClickHandle2Refresh}>
						重新加载
					</button>
				</div>
			)}
		</div>
	);
}

PageError.propTypes = {
	errorType: PropTypes.number,
	errorMsg: PropTypes.string
};

export default PageError;
