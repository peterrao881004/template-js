import * as loginSagas from "./login.saga";
import * as homeSagas from "./home.saga";
import { all, fork } from "redux-saga/effects";
import * as commonSagas from "./common.saga";

export default function* rootSaga() {
	yield all([
		fork(loginSagas.watchFetchLoginData),
		fork(homeSagas.watchFetchHomeData),
		fork(commonSagas.watchFetchInit)
	]);
}
