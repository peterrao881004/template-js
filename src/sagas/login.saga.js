import React from 'react'
import { takeEvery, put } from "redux-saga/effects";
import * as api from "@src/api";
import { requestSaga } from "./request.saga";
import { FETCH_LOGIN_DATA, RECEIVE_LOGIN_DATA } from "@types";
import { Toast } from 'antd-mobile'
import CountDownText from '@components/CountDownText'

function* fetchLoginData() {
	Toast.show({
		icon: 'loading',
		content: <CountDownText />,
		duration: 0,
	});
	try {
		const payload = yield requestSaga(api.fetchLoginData, {
			apiParam: {},
		});
		Toast.clear();
		yield put({ type: RECEIVE_LOGIN_DATA, payload });
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetchLoginData() {
	yield takeEvery(FETCH_LOGIN_DATA, fetchLoginData);
}
