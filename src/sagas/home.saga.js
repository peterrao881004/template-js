import React from 'react'
import { takeEvery, put } from "redux-saga/effects";
import * as api from "@src/api";
import { requestSaga } from "./request.saga";
import { FETCH_HOME_DATA, RECEIVE_HOME_DATA } from "@types";
import { Toast } from 'antd-mobile'
import CountDownText from '@components/CountDownText'

function* fetchHomeData() {
	Toast.show({
		icon: 'loading',
		content: <CountDownText />,
		duration: 0,
	});
	try {
		const payload = yield requestSaga(api.fetchHomeData, {
			apiParam: {},
		});
		Toast.clear();
		yield put({ type: RECEIVE_HOME_DATA, payload });
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetchHomeData() {
	yield takeEvery(FETCH_HOME_DATA, fetchHomeData);
}
