import React from 'react'
import { takeEvery, delay, put } from "redux-saga/effects";
import * as api from "@src/api";
import { requestSaga } from "./request.saga";
import { FETCH_INIT, UPDATE_INITIALIZE } from "@types";
import { Toast } from 'antd-mobile'
import CountDownText from '@components/CountDownText'

export function* fetchInit() {
	Toast.show({
		icon: 'loading',
		content: <CountDownText />,
		duration: 0,
	});
	try {
		yield delay(5000);
		const payload = yield requestSaga(api.fetchInit, {
			apiParam: {},
			errorLevel: 2,
			contentType: "a",
		});
		Toast.clear();
		yield put({ type: UPDATE_INITIALIZE, payload });
	} catch (e) {
		console.log(e);
	}
}

export function* watchFetchInit() {
	yield takeEvery(FETCH_INIT, fetchInit);
}