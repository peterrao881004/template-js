import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import Routers from "./routes";
import { store } from "./store";

const App = () => (
	<BrowserRouter>
		<Provider store={store}>
			<Routers />
		</Provider>
	</BrowserRouter>
);

export default App;
