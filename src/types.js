export function action(type, payload = {}) {
	return {
		type,
		payload,
	};
}

export const UPDATE_INITIALIZE = "UPDATE_INITIALIZE";
export const PAGE_INITIALIZED_ERROR = "PAGE_INITIALIZED_ERROR";
export const FETCH_INIT = "FETCH_INIT"; //页面初始化
export const FETCH_WORLD_DATA = "FETCH_WORLD_DATA"; //请求world数据
export const RECEIVE_WORLD_DATA = "RECEIVE_WORLD_DATA"; //接收world数据

export const FETCH_HOME_DATA = "FETCH_HOME_DATA"; //请求home数据
export const RECEIVE_HOME_DATA = "RECEIVE_HOME_DATA"; //接收home数据

export const FETCH_HOTELS_DATA = "FETCH_HOTELS_DATA"; //请求hotels数据
export const RECEIVE_HOTELS_DATA = "RECEIVE_HOTELS_DATA"; //接收hotels数据

export const FETCH_LOGIN_DATA = "FETCH_LOGIN_DATA"; //请求login数据
export const RECEIVE_LOGIN_DATA = "RECEIVE_LOGIN_DATA"; //接收login数据
