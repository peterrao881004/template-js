import React, { Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import routesConfig from "./config";
import PageLayout from "@base/page-layout";
import { generateRoute } from "./route-generator";
import '../styles/notFound.scss';

const routesData = generateRoute(routesConfig);

function Routers() {
	return (
		<Suspense fallback={<></>}>
			<Switch>
				{routesData.map((route, index) => {
					const { exact, path, component } = route;

					return (
						<Route
							key={`route-${index}`}
							exact={exact}
							path={path}
							render={(props) => {
								return <PageLayout {...props} component={component} />;
							}}
						/>
					);
				})}
				<Route
					render={() => {
						return <section className="error-body">
							<video preload="auto" className="background" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/396624/err.mp4" autoPlay muted loop></video>
							<div className="message">
								<h1 t="404">404</h1>
								<div className="bottom">
									<p>You have lost your way</p>
									<a href="/home">return home</a>
								</div>
							</div>
						</section>;
					}}
				/>
			</Switch>
		</Suspense>
	);
}
export default Routers;
