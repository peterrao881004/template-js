const routesConfig = [
	{
		path: "/login",
		page: "login",
	},
	{
		path: "/home",
		page: "home",
	},
	{
		path: "/",
		page: "test",
	},
];

export default routesConfig;
