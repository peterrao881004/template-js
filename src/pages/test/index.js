import React, { useEffect } from "react";
import { FETCH_INIT } from "@types";
import { useAction } from "@hooks";

import "./index.css";

function Home() {
	const fetchData = useAction(FETCH_INIT);
	
	useEffect(() => {
		document.title = "这是测试页面";
		fetchData();
	}, []);

	return (
		<div>test</div>
	);
}

export default Home;
