import React, { useEffect } from "react";
import { useAction, useStore } from "@hooks";
import { FETCH_LOGIN_DATA } from "@src/types";
import CyberpunkButton from '@components/CyberpunkButton'
import CyberpunkInput from '@components/CyberpunkInput'

import { useHistory } from "react-router-dom";
import "./index.css";

function Login() {
	const fetchData = useAction(FETCH_LOGIN_DATA);
	const { loginState } = useStore("loginReducer");
	const navigateTo = useHistory();

	useEffect(() => {
		document.title = "login";
		fetchData();
	}, []);

	if (!loginState) return null;
	return <div className="login">
		<div title="做最土的登录页" style={{ fontSize: 30, marginBottom: 20, display: 'flex' }}>
			<div className="neon" style={{ marginRight: 10 }} >CYBER</div>
			<div className="flux" >PUNK</div>
		</div>
		<div className="login-content">
			<CyberpunkInput placeholder="Account" />
			<CyberpunkInput placeholder="Password" />
			<CyberpunkButton onClick={() => {
				navigateTo.push('/home')
			}} content='Sign up' />
		</div>
		<h1 title="做最土的登录页" style={{ fontSize: 14 }} className="cyberpunk-title cyberpunk glitched">Welcome to cyberpunk</h1>
	</div>;
}

export default Login;
