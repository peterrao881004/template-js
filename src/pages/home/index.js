import React, { useEffect } from "react";
import { useAction, useStore } from "@hooks";
import { FETCH_HOME_DATA } from "@src/types";
import { CascaderView } from 'antd-mobile'
import "./index.css";

function Home() {
	const fetchData = useAction(FETCH_HOME_DATA);
	const { homeState } = useStore("homeReducer");

	useEffect(() => {
		document.title = "home";
		fetchData();
	}, []);

	if (!homeState) return null;
	return <div>
		<CascaderView
			options={homeState.result}
			onChange={(val, ext) => {
				console.log(val, ext.items)
			}}
		/>
	</div>;
}

export default Home;
