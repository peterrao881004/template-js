export const setCookie = (key, val, time) => {
	const date = new Date(); //获取当前时间
	const expiresDays = time; //将date设置为n天以后的时间
	date.setTime(date.getTime() + expiresDays * 24 * 3600 * 1000); //格式化为cookie识别的时间
	document.cookie = key + "=" + val + ";expires=" + date.toGMTString() + ";path=/"; //设置cookie
};

/**
 * 参数提取
 * @param search    字符串（默认当前路径）
 * @param key       参数属性名
 */
export const extractParameter = (key, search) => {
	const searchStr = search || location.search;
	const pattern = new RegExp("[?&]" + key + "=([^&]+)", "g");
	const matcher = pattern.exec(searchStr);

	return matcher ? decodeURIComponent(matcher[1]) : "";
};

/**
 * 去除资源文件协议头
 * @param url
 * @returns {*}
 */
export const removeProtocol = url => {
	const regex = /^http(s)?:/;
	if (regex.test(url)) {
		return url.replace(regex, "");
	} else {
		return url;
	}
};

/**
 * isBrowser
 * @returns {boolean}
 */
export const isBrowser = () => {
	return typeof window != "undefined";
};