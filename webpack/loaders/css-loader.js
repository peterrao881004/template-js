const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssNested = require("postcss-nested");
const postcssPresetEnv = require("postcss-preset-env");
const px2rem = require("postcss-plugin-px2rem");
const { APP_ENV } = require("../define");

module.exports = {
	test: /\.css$/,
	include: [path.resolve(__dirname, "../../src"), path.resolve(__dirname, "../../node_modules")],
	use: [
		APP_ENV === "local" ? "style-loader" : MiniCssExtractPlugin.loader,
		"css-loader",
		{
			loader: "postcss-loader",
			options: {
				ident: "postcss",
				plugins: () => [
					px2rem({
						rootValue: 10,
						selectorBlackList: [".gm-badge"],
						propBlackList: ["border"]
					}),
					postcssNested(),
					postcssPresetEnv({
						stage: 3,
						browsers: ["last 3 versions"],
						features: {
							"nesting-rules": true,
						},
					}),
				],
			},
		},
	],
};
