const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssNested = require("postcss-nested");
const postcssPresetEnv = require("postcss-preset-env");
const px2rem = require("postcss-plugin-px2rem");
const { APP_ENV } = require("../define");

module.exports = {
	test: /\.s[ac]ss$/i,
	include: [path.resolve(__dirname, "../../src")],
	use: [
		APP_ENV === "local" ? "style-loader" : MiniCssExtractPlugin.loader,
		"css-loader",
        "sass-loader",
	],
};
