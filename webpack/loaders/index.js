const cssLoader = require("./css-loader");
const sassLoader = require("./sass-loader");
const babelLoader = require("./babel-loader");
const urlLoader = require("./url-loader");
const path = require("path");

module.exports = {
	rules: [
		{
			test: /node_modules\/.+\.svg$/,
			use: {
				loader: 'file-loader',
			},
		},
		{
			test: /\.svg$/,
			loader: 'svg-sprite-loader',
			exclude: [/node_modules/],
		},
		babelLoader,
		sassLoader,
		cssLoader,
		urlLoader,
	],
};
